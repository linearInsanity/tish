:ClrHome
:Lbl ST
:Disp " TISH: TI SHELL "
:Disp " ============== "
:While 1=1
:Input ")",Str1                    #String 1 is where the Shell Input is Stored
:If Str1="ADD"                     #===Addition Program===
:Then                              #Adds Two Arguments
:Input "A:",A
:Input "B:",B
:Disp A+B
:End
:If Str1="SUB"                     #===Subtraction Program===
:Then                              #Subtracts Two Arguments
:Input "A:",A
:Input "B:",B
:Disp A-B
:End
:If Str1="MUL"                     #===Multiplication Program===
:Then                              #Multiplies Two Arguments
:Input "A:",A
:Input "B:",B
:Disp A*B
:End
:If Str1="DIV"                     #===Division Program===
:Then                              #Divides Two Arguments
:Input "A:",A
:Input "B:",B
:Disp A/B
:End
:If Str1="AGEN"                    #===Answer Generator===
:Then                              #Joke Program, Generates Randoms A-E
:prgmAGEN                          #File: agen.8xp
:End
:If str1="TANO"                    #===Ed-like Text Editor===
:Then                              #Named After Nano (Ti-nANO)
:prgmTANO
:End
:If Str1="ECHO Str1"               #===String 1 Echo===
:Then
:Disp Str1
:End
:If Str1="ECHO Str2"               #===String 2 Echo===
:Then
:Disp Str2
:End
:If Str1="ECHO Str3"               #===String 3 Echo===
:Then
:Disp Str3
:End
:If Str1="ECHO Str4"               #===String 4 Echo===
:Then
:Disp Str4
:End
:If Str1="ECHO Str5"               #===String 5 Echo===
:Then
:Disp Str5
:End
:If Str1="ECHO Str6"               #===String 6 Echo===
:Then
:Disp Str6
:End
:If Str1="ECHO Str7"               #===String 7 Echo===
:Then
:Disp Str7
:End
:If Str1="ECHO Str8"               #===String 8 Echo===
:Then
:Disp Str8
:End
:End
