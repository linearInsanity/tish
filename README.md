Tish (Texas Instruments SHell)
======

###About Tish

Tish is a Shell Environment for the TI(84) calculator. It comes bundled with some user tools as well. Many of the global variables are made use of in tish and many need to be flushed before using a program. Some of the tools within tish include:

```
- Ed-like Text Editor (Tano)
- Four Function Scripts (+|-|*|/)
- Interfacing to Exisiting TI tools
```

###Limitations

######Source Code

The source code cannot be transfered onto GitHub 1:1 because of the nature of TI-Basic. For this reason, the file "disambig.txt" is included to resolve some of the ambiguities with TI-Basic commands and text form.

######Implementation

Due to the nature of the TI calculator, if an error is thrown at any time, Tish will quit to the main screen.

###Future Plans

These features are being planned to be implemented within Tish:

```
- Scripting Language (Lisp-Based)
- Files (Rather than Str1, Str2, etc.)
- Graph Mode Shell (For better looking drawn shell)
```
